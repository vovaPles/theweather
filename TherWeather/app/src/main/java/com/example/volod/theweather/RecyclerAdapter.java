package com.example.volod.theweather;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    ArrayList<Data> strings;

    public class ViewHolder extends RecyclerView.ViewHolder implements com.example.volod.theweather.ViewHolder {

        TextView dayName, cityTemp, cityCloud;

        public ViewHolder(View itemView) {
            super(itemView);

            dayName = (TextView) itemView.findViewById(R.id.dayName);
            cityCloud = (TextView) itemView.findViewById(R.id.cloud);
            cityTemp = (TextView) itemView.findViewById(R.id.temp);
        }
    }

    public RecyclerAdapter(ArrayList<Data> strings) {
        this.strings = strings;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_item, parent, false);
        ViewHolder mHolder = new ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return mHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder holder, int position) {

        holder.dayName.setText(strings.get(position).dayName);
        holder.cityTemp.setText(strings.get(position).cityTemp);
        holder.cityCloud.setText(strings.get(position).cityCloud);
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
