package com.example.volod.theweather;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class FragmentWeather extends android.app.Fragment {
    MyTask myTask;
    String cityId;
    TextView textName, textDate, textSpeed, textCloud, textTemp, textRiseSet;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, null);

        textName = (TextView) view.findViewById(R.id.textName);
        textDate = (TextView) view.findViewById(R.id.textDate);
        textSpeed = (TextView) view.findViewById(R.id.textSpeed);
        textCloud = (TextView) view.findViewById(R.id.textCloud);
        textTemp = (TextView) view.findViewById(R.id.textTemp);
        textRiseSet = (TextView) view.findViewById(R.id.textRiseSet);

        String id = "709611";

        if (getCityId() != null) id = getCityId();

        myTask = new MyTask();
        myTask.execute("weather", id);
        parsingWeather();
        return view;
    }

    void parsingWeather() {
        String json  = null;
        WeatherBody body = null;
        Gson gson = new Gson();
        try {
            json = myTask.get();
            body = gson.fromJson(json, WeatherBody.class);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        Date date = new Date();
        date.setTime((long) body.dt * 1000);
        String dt = new SimpleDateFormat("MMM d, yyyy").format(date);
        String sunRise = converter(body.sys.sunrise);
        String sunSet = converter(body.sys.sunset);
        textName.setText(body.name);
        textDate.setText(dt);
        textSpeed.setText(String.valueOf(body.wind.speed));
        textCloud.setText(body.weather[0].description);
        textRiseSet.setText(getString(R.string.riseset)+ "  " + sunRise + "|" + sunSet);
        textTemp.setText(String.valueOf(body.main.temp) + "°C");
    }

    String converter(int i) {
        Date date2 = new Date();
        date2.setTime((long) i * 1000);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        String sunTime = simpleDateFormat.format(date2).toString();
        return sunTime;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }
}
