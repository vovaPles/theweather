
package com.example.volod.theweather;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class FragmentSetting extends android.app.Fragment {
    String[] cities = {"Choose city", "Drogobych", "Truskavest", "Lviv"};
    private String[] ids = {"709611", "691179", "702550"};
    String cityId;

    public interface onSetCityIdListener {
        void setCityId(String s);
    }

    onSetCityIdListener setCityIdListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            setCityIdListener = (onSetCityIdListener) activity;
        }catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must implement onSetCityIdListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting, null);

        Spinner spinner = (Spinner) view.findViewById(R.id.spiner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, cities);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FragmentWeather weather = new FragmentWeather();
                MyTask myTask = new MyTask();
                if (position > 0) {
                    setCityIdListener.setCityId(ids[position-1]);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }
}
