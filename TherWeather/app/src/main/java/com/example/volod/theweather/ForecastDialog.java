package com.example.volod.theweather;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ForecastDialog extends DialogFragment {

    TextView textName, textTemp, textCloud, textSpeed , textRiseSet, textDate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Weather");
        View view = inflater.inflate(R.layout.fragment_weather, null);

        textName = (TextView) view.findViewById(R.id.textName);
        textTemp = (TextView) view.findViewById(R.id.textTemp);
        textCloud = (TextView) view.findViewById(R.id.textCloud);
        textSpeed = (TextView) view.findViewById(R.id.textSpeed);
        textDate = (TextView) view.findViewById(R.id.textDate);

        textName.setText(getArguments().getString("name"));
        textTemp.setText(getArguments().getString("temp"));
        textCloud.setText(getArguments().getString("cloud"));
        textSpeed.setText("Wind speed " + getArguments().getString("speed"));
        textDate.setText(getArguments().getString("date"));
        return view;
    }
}
