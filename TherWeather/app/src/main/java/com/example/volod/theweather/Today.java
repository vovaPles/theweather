package com.example.volod.theweather;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class Today extends AppCompatActivity implements FragmentSetting.onSetCityIdListener {

    BottomNavigationView bottomNavigationView;
    FragmentWeather weather;
    FragmentForecast forecast;
    String string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.today);

        bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bnView);
        bottomNavigationView.getMenu().getItem(0).setChecked(true);

        final FragmentTransaction transaction = getFragmentManager().beginTransaction();
        weather = new FragmentWeather();
        transaction.add(R.id.fragment_container, weather);
        transaction.commit();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                forecast = new FragmentForecast();
                FragmentSetting setting = new FragmentSetting();
                switch (item.getItemId()) {
                    case R.id.today:
                        item.setChecked(true);
                        fTransaction(weather);
                        break;
                    case R.id.fiveDays:
                        item.setChecked(true);
                        fTransaction(forecast);
                        break;
                    case R.id.setting:
                        item.setChecked(true);
                        fTransaction(setting);
                        break;
                }
                return false;
            }
        });
    }
    public void fTransaction (Fragment fragment) {
        if (string != null){
            weather.setCityId(string);
            forecast.setCityId(string);
        }
        FragmentTransaction transactionFragment = getFragmentManager().beginTransaction();
        transactionFragment.replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public void setCityId(String s) {
        bottomNavigationView.getMenu().getItem(0).setChecked(true);
        if (s != null) {
            string = s;
            fTransaction(weather);
        }
    }
}
