package com.example.volod.theweather;

public class Data {

    String dayName, cityTemp, cityCloud;

    Data(String _dayName, String _cityTemp, String _cityCloud) {

        dayName = _dayName;
        cityTemp = _cityTemp;
        cityCloud = _cityCloud;
    }
}
