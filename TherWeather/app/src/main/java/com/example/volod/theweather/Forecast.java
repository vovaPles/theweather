package com.example.volod.theweather;

public class Forecast {

    List[] list;
    City city;
    int cnt;
}

class City {
    String name;
}

class List {
    int dt;
    Fwind wind;
    Fmain main;
    Fweather[] weather;
}

class Fmain {
    Float temp;
}
class Fwind {
    Float speed;
}

class Fweather {
    String description;
}