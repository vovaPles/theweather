package com.example.volod.theweather;

import android.os.AsyncTask;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;


public class MyTask extends AsyncTask<String, Void, String>{

    private OkHttpClient client = new OkHttpClient();

    @Override
    protected String doInBackground(String... params) {

        final Request request = new Request.Builder()
                .url("http://api.openweathermap.org/data/2.5/" + params[0] + "?id=" + params[1]
                        + "&appid=766638ff8a52ac64a725ea8d5d0504c2&units=metric")
                .build();
        try {

            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {return null;}
            return response.body().string();

        } catch (Exception e) {e.printStackTrace();}
        return null;
    }
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
