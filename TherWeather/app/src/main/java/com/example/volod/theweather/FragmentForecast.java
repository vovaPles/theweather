package com.example.volod.theweather;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class FragmentForecast extends android.app.Fragment {
    String cityId;
    MyTask myTask;
    RecyclerView rView;
    TextView cityName;
    LinearLayoutManager layoutManager;
    ArrayList<Data> forecastData;
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> dayDate, speed, cloud, temp;
    RecyclerAdapter recyclerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forecast, null);
        cityName = (TextView) view.findViewById(R.id.cityName);
        rView = (RecyclerView) view.findViewById(R.id.rView);
        rView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);

        String id = "709611";
        if (getCityId() != null) id = getCityId();

        myTask = new MyTask();
        myTask.execute("forecast", id);
        parsingForecast();
        return view;
    }

    public void parsingForecast() {
        Forecast forecast = null;
        String json = null, d;
        Date date;
        Gson gson = new Gson();

        try {
            json = myTask.get();
            forecast = gson.fromJson(json, Forecast.class);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        cityName.setText(forecast.city.name);
        name.add(forecast.city.name);

        forecastData = new ArrayList<>();
        dayDate = new ArrayList<>();
        cloud = new ArrayList<>();
        speed = new ArrayList<>();
        temp = new ArrayList<>();
        int i = 0;
        if (forecast.cnt > 0) {
            while (i < forecast.cnt) {
                if (i == 0 || i % 8 == 0) {
                    date = new Date();
                    date.setTime((long) forecast.list[i].dt * 1000);
                    d = new SimpleDateFormat("EEEE").format(date);
                    forecastData.add(new Data(d, String.valueOf(forecast.list[i].main.temp),
                            forecast.list[i].weather[0].description));
                    name.add(forecast.city.name);
                    dayDate.add(new SimpleDateFormat("MMM d, yyyy").format(date));
                    temp.add(String.valueOf(forecast.list[i].main.temp));
                    cloud.add(forecast.list[i].weather[0].description);
                    speed.add(String.valueOf(forecast.list[i].wind.speed));
                }
                i++;
            }
            rView.setLayoutManager(layoutManager);
            recyclerAdapter = new RecyclerAdapter(forecastData);
            rView.setAdapter(recyclerAdapter);

            rView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rView,
                    new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    ForecastDialog forecastDialog = new ForecastDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString("name", name.get(position + 1));
                    bundle.putString("speed", speed.get(position));
                    bundle.putString("cloud", cloud.get(position));
                    bundle.putString("temp", temp.get(position));
                    bundle.putString("date", dayDate.get(position));
                    forecastDialog.setArguments(bundle);
                    forecastDialog.show(getFragmentManager(), "forecast");
                    Log.d("boba", String.valueOf(position) + "  " + String.valueOf(name.size())
                        +"  "+ String.valueOf(name.size()));
                }

                @Override
                public void onLongItemClick(View view, int position) {
                // ne nado poki
                }
            }));
        }

    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }
}
